﻿using System;

namespace week_3_exercise_16
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***************************");
            Console.WriteLine("**** Welcome to my App ****");
            Console.WriteLine("***************************");
            Console.WriteLine("");
            Console.WriteLine("***************************");
            Console.WriteLine("What is your name?");
            Console.WriteLine("");

            var usrName = Console.ReadLine();
            Console.WriteLine($"Hello, {usrName}");
        }
    }
}